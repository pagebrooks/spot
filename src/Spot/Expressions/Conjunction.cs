﻿namespace Spot.Expressions
{
    public enum Conjunction { And = 0, Or = 1 }
}