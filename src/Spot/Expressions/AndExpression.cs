﻿namespace Spot.Expressions
{
    public class AndExpression : GroupExpression, IExpression
    {
        public AndExpression(IExpression[] exprs) : base(Conjunction.And, exprs)
        {
        }

        public override void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}