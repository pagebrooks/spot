﻿using System;
using System.Linq.Expressions;

namespace Spot.Expressions
{
    public class BinaryExpression: BaseExpression, IExpression
    {
        public Operator Op { get; }
        public string Left { get; }
        public object Right { get; }

        public BinaryExpression(Operator op, string left, object right)
        {
            Op = op;
            Left = left;
            Right = right;
        }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}