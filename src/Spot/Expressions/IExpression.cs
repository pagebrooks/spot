﻿namespace Spot.Expressions
{
    public interface IExpression
    {
        void Accept(IExpressionVisitor visitor);
    }
}