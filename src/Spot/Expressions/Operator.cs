﻿namespace Spot.Expressions
{
    public enum Operator { Equals, NotEquals, LessThan, LessThanEqualTo, GreaterThan, GreaterThanEqualTo};
}