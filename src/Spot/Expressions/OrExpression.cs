﻿namespace Spot.Expressions
{
    public class OrExpression : GroupExpression
    {
        public OrExpression(IExpression[] exprs) : base(Conjunction.Or, exprs)
        {
        }

        public override void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}