﻿using System;
using System.Linq.Expressions;

namespace Spot.Expressions
{
    public class LikeExpression: BaseExpression
    {
        public string Left { get; }
        public object Right { get; }

        public LikeExpression(string left, object right)
        {
            Left = left;
            Right = right;
        }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}