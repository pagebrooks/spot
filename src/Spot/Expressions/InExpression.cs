using System;
using System.Linq.Expressions;

namespace Spot.Expressions
{
    public class InExpression : BaseExpression
    {
        public string Left { get; }
        public object[] Values { get; }

        public InExpression(string left, params object[] values)
        {
            Left = left;
            Values = values;
        }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}