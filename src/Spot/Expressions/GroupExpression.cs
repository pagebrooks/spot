﻿namespace Spot.Expressions
{
    public abstract class GroupExpression : IExpression
    {
        public Conjunction Conjunction { get; }
        public IExpression[] Expressions { get; }

        protected GroupExpression(Conjunction conjunction, IExpression[] exprs)
        {
            Conjunction = conjunction;
            Expressions = exprs;
        }

        public abstract void Accept(IExpressionVisitor visitor);
    }
}