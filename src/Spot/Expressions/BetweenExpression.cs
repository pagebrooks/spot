﻿using System;
using System.Linq.Expressions;

namespace Spot.Expressions
{
    public class BetweenExpression : BaseExpression, IExpression
    {
        public string Left { get; }
        public object Value1 { get; }
        public object Value2 { get; }

        public BetweenExpression(string left, object value1, object value2) : base()
        {
            Left = left;
            Value1 = value1;
            Value2 = value2;
        }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}