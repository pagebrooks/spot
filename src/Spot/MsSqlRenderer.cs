﻿using System;
using System.Text;
using System.IO;
using Spot.Expressions;
using System.Collections.Generic;

namespace Spot
{
    public class MsSqlRenderer : IExpressionVisitor
    {
        private readonly MsSqlContext _context;
        private readonly TextWriter _writer;

        public MsSqlRenderer(MsSqlContext context, TextWriter writer)
        {
            _context = context;
            _writer = writer;
        }

        private void Write(string data)
        {
            _writer.Write(data);
        }

        private static string RenderFieldName(string name)
        {
            return $"[{name}]";
        }

        private static string RenderTableName(string table, string schema = null)
        {
            if(!string.IsNullOrWhiteSpace(schema))
            {
                return $"[{schema}].[{table}]";
            }
            else
            {
                return $"[{table}]";
            }
        }

        private static string GetOperator(Operator op)
        {
            switch (op)
            {
                case Operator.Equals: return "=";
                case Operator.NotEquals: return "<>";
                case Operator.GreaterThan: return ">";
                case Operator.GreaterThanEqualTo: return ">=";
                case Operator.LessThan: return "<";
                case Operator.LessThanEqualTo: return "<=";
                default: throw new ArgumentException("Unexpected operator");
            }
        }

        public void Visit(BetweenExpression expr)
        {
            var name1 = _context.AddVariable(expr.Left, expr.Value1);
            var name2 = _context.AddVariable(expr.Left, expr.Value2);
            Write($"{RenderFieldName(expr.Left)} BETWEEN {name1} AND {name2}");
        }

        public void Visit(BinaryExpression expr)
        {
            var name = _context.AddVariable(expr.Left, expr.Right);
            Write($"{RenderFieldName(expr.Left)} {GetOperator(expr.Op)} {name}");
        }

        public void Visit(LikeExpression expr)
        {
            var name = _context.AddVariable(expr.Left, expr.Right);
            Write($"{RenderFieldName(expr.Left)} LIKE {name}");
        }

        public void Visit(InExpression expr)
        {
            Write($"{RenderFieldName(expr.Left)} IN (");
            for (int i = 0; i < expr.Values.Length; i++)
            {
                var name = _context.AddVariable(expr.Left, expr.Values[i]);
                if (i > 0)
                {
                    Write(", ");
                }

                Write(name);
            }

            Write(")");
        }

        public void Visit(GroupExpression expr)
        {
            string conjunctionString = null;
            switch (expr.Conjunction)
            {
                case Conjunction.And:
                    conjunctionString = "AND";
                    break;
                case Conjunction.Or:
                    conjunctionString = "OR";
                    break;
                default: throw new ArgumentException("Unexpected conjunction");
            }

            Write("(");
            for (int i = 0; i < expr.Expressions.Length; i++)
            {
                if (i > 0)
                {
                    Write(conjunctionString);
                    Write(" ");
                }

                expr.Expressions[i].Accept(this);
                if (i < expr.Expressions.Length - 1)
                {
                    Write(" ");
                }
            }

            Write(")");
        }

        public void RenderInsert<T>(T entity)
        {
            var t = typeof(T);
            var mapper = SpotSettings.GetClassMapper<T>();

            Write("INSERT INTO ");
            Write(RenderTableName(mapper.TableName, mapper.SchemaName));
            Write(" (");

            int fieldCount = 0;
            foreach (var pair in mapper.Mappings)
            {
                if (fieldCount > 0)
                {
                    Write(", ");
                }

                if (pair.Value.FieldType == FieldType.Field)
                {
                    Write(RenderFieldName(pair.Value.ColumnName));
                    fieldCount++;
                }
            }

            Write(") VALUES (");
            fieldCount = 0;
            foreach (var pair in mapper.Mappings)
            {
                if (fieldCount > 0)
                {
                    Write(", ");
                }

                if (pair.Value.FieldType == FieldType.Field)
                {
                    var prop = t.GetProperty(pair.Key);
                    var propVal = prop.GetValue(entity);

                    var varName = _context.AddVariable(pair.Value.ColumnName, propVal);
                    Write(varName);
                    fieldCount++;
                }
            }

            Write(")");
        }

        public void RenderUpdate<T>(T entity)
        {
            var t = typeof(T);
            var mapper = SpotSettings.GetClassMapper<T>();

            Write("UPDATE ");
            Write(RenderTableName(mapper.TableName, mapper.SchemaName));
            Write(" SET ");

            int fieldCount = 0;
            var predicate = new StringBuilder();
            foreach (var pair in mapper.Mappings)
            {
                if (fieldCount > 0)
                {
                    Write(", ");
                }

                if (pair.Value.FieldType == FieldType.Field)
                {
                    var prop = t.GetProperty(pair.Key);
                    var propVal = prop.GetValue(entity);

                    Write(RenderFieldName(pair.Value.ColumnName));
                    Write(" = ");
                    Write(_context.AddVariable(pair.Value.ColumnName, propVal));

                    fieldCount++;
                }
                else if (pair.Value.FieldType == FieldType.IdentityKey || pair.Value.FieldType == FieldType.Key)
                {
                    var prop = t.GetProperty(pair.Key);
                    var propVal = prop.GetValue(entity);
                    predicate.Append(" WHERE ");
                    predicate.Append(RenderFieldName(pair.Value.ColumnName));
                    predicate.Append(" = ");
                    predicate.Append(_context.AddVariable(pair.Value.ColumnName, propVal));
                }
            }

            Write(predicate.ToString());
        }

        public void RenderDelete<T>(object id)
        {
            var t = typeof(T);
            var mapper = SpotSettings.GetClassMapper<T>();

            Write("DELETE FROM ");
            Write(RenderTableName(mapper.TableName, mapper.SchemaName));
            Write(" WHERE ");

            foreach (var pair in mapper.Mappings)
            {
                if (pair.Value.FieldType == FieldType.IdentityKey || pair.Value.FieldType == FieldType.Key)
                {
                    Write(pair.Value.ColumnName);
                    Write(" = ");
                    Write(_context.AddVariable(pair.Value.ColumnName, id));
                    break;
                }
            }
        }

        public void RenderSelectSingle<T>(object id)
        {
            var t = typeof(T);
            var mapper = SpotSettings.GetClassMapper<T>();

            Write("SELECT ");
            var fieldCount = 0;
            foreach (var pair in mapper.Mappings)
            {
                if (fieldCount > 0)
                {
                    Write(", ");
                }

                Write(RenderFieldName(pair.Value.ColumnName));
                fieldCount++;
            }

            Write(" FROM ");
            Write(RenderTableName(mapper.TableName, mapper.SchemaName));
            Write(" WHERE ");

            foreach (var pair in mapper.Mappings)
            {
                if (pair.Value.FieldType == FieldType.IdentityKey || pair.Value.FieldType == FieldType.Key)
                {
                    Write(RenderFieldName(pair.Value.ColumnName));
                    Write(" = ");
                    Write(_context.AddVariable(pair.Value.ColumnName, id));
                    break;
                }
            }
        }

        public void RenderSelect<T>(IExpression expr)
        {
            var t = typeof(T);
            var mapper = SpotSettings.GetClassMapper<T>();
            var fields = FieldHelper.GetFields(t);

            Write("SELECT ");
            var fieldCount = 0;
            foreach (var pair in mapper.Mappings)
            {
                if (fieldCount > 0)
                {
                    Write(", ");
                }

                Write(RenderFieldName(pair.Value.ColumnName));
                fieldCount++;
            }

            Write(" FROM ");
            Write(RenderTableName(mapper.TableName, mapper.SchemaName));


            if (expr != null)
            {
                Write(" WHERE ");
                expr.Accept(this);
            }
        }
    }
}