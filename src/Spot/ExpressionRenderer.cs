using Spot.Expressions;

namespace Spot 
{
    public interface IExpressionVisitor 
    {
        void Visit(BinaryExpression expression);
        void Visit(GroupExpression expression);
        void Visit(LikeExpression expression);
        void Visit(InExpression expression);
        void Visit(BetweenExpression expression);
    }
}