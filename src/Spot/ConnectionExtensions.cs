﻿using System.Data.SqlClient;
using Spot.Expressions;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Data.Common;

namespace Spot
{
    public class Command
    {
        public string Sql { get; set; }
        public Dictionary<string, object> Variables { get; set; }
    }

    public static class ConnectionExtensions
    {
        public static Command GetList<T>(IExpression expr)
        {
            var sb = new StringBuilder();
            var writer = new StringWriter(sb);
            var sqlContext = new MsSqlContext();
            var sqlRenderer = new MsSqlRenderer(sqlContext, writer);
            sqlRenderer.RenderSelect<T>(expr);

            var command = new Command
            {
                Sql = sb.ToString(),
                Variables = sqlContext.Variables
            };

            return command;
        }

        public static Command Get<T>(object id)
        {
            var sb = new StringBuilder();
            var writer = new StringWriter(sb);
            var sqlContext = new MsSqlContext();
            var sqlRenderer = new MsSqlRenderer(sqlContext, writer);
            sqlRenderer.RenderSelectSingle<T>(id);

            var command = new Command
            {
                Sql = sb.ToString(),
                Variables = sqlContext.Variables
            };

            return command;
        }

        public static Command Delete<T>(object id)
        {
            var sb = new StringBuilder();
            var writer = new StringWriter(sb);
            var sqlContext = new MsSqlContext();
            var sqlRenderer = new MsSqlRenderer(sqlContext, writer);
            sqlRenderer.RenderDelete<T>(id);

            var command = new Command
            {
                Sql = sb.ToString(),
                Variables = sqlContext.Variables
            };

            return command;
        }

        public static Command Insert<T>(T entity)
        {
            var sb = new StringBuilder();
            var writer = new StringWriter(sb);
            var sqlContext = new MsSqlContext();
            var sqlRenderer = new MsSqlRenderer(sqlContext, writer);
            sqlRenderer.RenderInsert<T>(entity);

            var command = new Command
            {
                Sql = sb.ToString(),
                Variables = sqlContext.Variables
            };

            return command;
        }

        public static Command Update<T>(T entity)
        {
            var sb = new StringBuilder();
            var writer = new StringWriter(sb);
            var sqlContext = new MsSqlContext();
            var sqlRenderer = new MsSqlRenderer(sqlContext, writer);
            sqlRenderer.RenderUpdate<T>(entity);

            var command = new Command
            {
                Sql = sb.ToString(),
                Variables = sqlContext.Variables
            };

            return command;
        }
    }
}