﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Spot
{
    public static class SpotSettings
    {
        public static Dictionary<Type, IClassMapper> ClassMappers = new Dictionary<Type, IClassMapper>();
  
        public static void ResetClassMappers()
        {
            ClassMappers.Clear();
        }

        public static IClassMapper GetClassMapper<T>()
        {
            var type = typeof(T);
            if (!ClassMappers.ContainsKey(type))
            {
                var mapper = new ClassMapper<T>();
                mapper.Map();
                ClassMappers.Add(type, mapper);
                return mapper;
            }

            return ClassMappers[type];
        }

        public static string GetColumnName<T>(Expression<Func<T, object>> expr)
        {
            var mapper = GetClassMapper<T>();
            var name = mapper.Mappings[FieldHelper.GetName(expr)].ColumnName;
            return name;
        }
    }
}
