﻿using System.Collections.Generic;

namespace Spot
{
    public class MsSqlContext : IRenderContext
    {
        private int _count;
        public Dictionary<string, object> Variables { get; }

        public MsSqlContext()
        {
            Variables = new Dictionary<string, object>();
        }

        public string AddVariable(string baseName, object value)
        {
            var variable = $"@{baseName}_{_count++}";
            Variables.Add(variable, value);
            return variable;
        }

        public void Reset()
        {
            Variables.Clear();
            _count = 0;
        }
    }
}