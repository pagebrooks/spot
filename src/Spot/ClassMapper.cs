﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;

namespace Spot
{
    public interface IClassMapper
    {
        void Map();
        Dictionary<string, Mapping> Mappings { get; }
        string TableName { get; }
        string SchemaName { get; }
    }

    public class ClassMapper<T> : IClassMapper
    {
        private readonly Type _type;
        private readonly PropertyInfo[] _fields;
        public string TableName { get; private set; }
        public string SchemaName { get; private set; }
        public Dictionary<string, Mapping> Mappings { get; private set; }

        public ClassMapper()
        {
            _type = typeof(T);
            _fields = FieldHelper.GetFields(_type);
            Mappings = new Dictionary<string, Mapping>();
        }

        protected void Table(string name)
        {
            TableName = name;
        }

        protected void Schema(string schema)
        {
            SchemaName = schema;
        }

        protected void Column(string columnName, FieldType fieldType, Expression<Func<T, object>> expr)
        {
            MapColumn(FieldHelper.GetName(expr), fieldType, columnName);
        }

        private void MapColumn(string columnName, FieldType fieldType, string propertyName)
        {
            Mappings.Add(propertyName, new Mapping { ColumnName = columnName, FieldType = fieldType });
        }

        public virtual void Map()
        {
            if (string.IsNullOrWhiteSpace(TableName))
            {
                Table(_type.Name);
            }

            bool firstIdFieldFound = false;
            foreach (var field in _fields)
            {
                if (!Mappings.ContainsKey(field.Name))
                {
                    var fieldType = FieldType.Field;
                    var dataType = field.PropertyType;
                    if (!firstIdFieldFound
                        && field.Name.EndsWith("id", StringComparison.InvariantCultureIgnoreCase)
                        && IsAutoIncrementingType(dataType))
                    {
                        fieldType = FieldType.IdentityKey;
                        firstIdFieldFound = true;
                    }

                    MapColumn(field.Name, fieldType, field.Name);
                }

            }
        }

        private bool IsAutoIncrementingType(Type t)
        {
            if (t == typeof(int))
            {
                return true;
            }

            if (t == typeof(ushort))
            {
                return true;
            }

            if (t == typeof(uint))
            {
                return true;
            }

            if (t == typeof(long))
            {
                return true;
            }

            if (t == typeof(ulong))
            {
                return true;
            }

            return false;
        }
    }

    public class Mapping
    {
        public string ColumnName { get; set; }
        public FieldType FieldType { get; set; }
    }

    public enum FieldType { IgnoreField = 0, Field = 1, Key = 2, IdentityKey = 3 }
}
