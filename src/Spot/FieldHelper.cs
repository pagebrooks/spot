﻿using System;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;

namespace Spot
{
    public static class FieldHelper
    {
        public static string GetName(LambdaExpression expr)
        {
            var body = expr.Body as MemberExpression;
            if (body == null)
            {
                var ubody = (UnaryExpression)expr.Body;
                body = ubody.Operand as MemberExpression;
            }

            return body.Member.Name;
        }

        public static PropertyInfo[] GetFields(Type t)
        {
            return t.GetProperties(BindingFlags.Instance | BindingFlags.Public);
        }
    }
}