﻿namespace Spot
{
    public interface IRenderContext
    {
        string AddVariable(string baseName, object value);
    }
}