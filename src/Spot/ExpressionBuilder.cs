﻿using System;
using linq = System.Linq.Expressions;
using Spot.Expressions;

namespace Spot
{
    public static class ExpressionBuilder
    {
        public static GroupExpression And(params IExpression[] exprs)
        {
            return new AndExpression(exprs);
        }

        public static GroupExpression Or(params IExpression[] exprs)
        {
            return new OrExpression(exprs);
        }

        public static BinaryExpression Eq<T>(linq.Expression<Func<T, object>> expr, object value)
        {
            var name = SpotSettings.GetColumnName(expr);
            return new BinaryExpression(Operator.Equals, name, value);
        }

        public static BinaryExpression Ne<T>(linq.Expression<Func<T, object>> expr, object value)
        {
            var name = SpotSettings.GetColumnName(expr);
            return new BinaryExpression(Operator.NotEquals, name, value);
        }

        public static BinaryExpression Gt<T>(linq.Expression<Func<T, object>> expr, object value)
        {
            var name = SpotSettings.GetColumnName(expr);
            return new BinaryExpression(Operator.GreaterThan, FieldHelper.GetName(expr), value);
        }

        public static BinaryExpression Gte<T>(linq.Expression<Func<T, object>> expr, object value)
        {
            var name = SpotSettings.GetColumnName(expr);
            return new BinaryExpression(Operator.GreaterThanEqualTo, name, value);
        }

        public static BinaryExpression Lt<T>(linq.Expression<Func<T, object>> expr, object value)
        {
            var name = SpotSettings.GetColumnName(expr);
            return new BinaryExpression(Operator.LessThan, name, value);
        }

        public static BinaryExpression Lte<T>(linq.Expression<Func<T, object>> expr, object value)
        {
            var name = SpotSettings.GetColumnName(expr);
            return new BinaryExpression(Operator.LessThanEqualTo, name, value);
        }

        public static BetweenExpression Between<T>(linq.Expression<Func<T, object>> expr, object value1, object value2)
        {
            var name = SpotSettings.GetColumnName(expr);
            return new BetweenExpression(name, value1, value2);
        }
    }
}