using System;
using Xunit;
using System.Text;
using System.IO;
using Spot;

namespace Spot.Tests
{
    public class ExpressionBuilderTests
    {
        [Fact]
        public void ExpressionBuilder_Simple_Select()
        {
            var personId = ExpressionBuilder.Eq<Person>(p => p.PersonId, 100);
            var age1 = ExpressionBuilder.Gt<Person>(p => p.Age, 10);
            var age2 = ExpressionBuilder.Lt<Person>(p => p.Age, 50);
            var between = ExpressionBuilder.Between<Person>(p => p.Age, 60, 65);
            var and = ExpressionBuilder.And(personId, age1, age2, between);

            var command = ConnectionExtensions.GetList<Person>(and);

            var sql = "SELECT [PersonId], [Age], [FirstName], [LastName], [DateOfBirth] FROM [Person] WHERE ([PersonId] = @PersonId_0 AND [Age] > @Age_1 AND [Age] < @Age_2 AND [Age] BETWEEN @Age_3 AND @Age_4)";

            Assert.Equal(sql, command.Sql);
            Assert.Equal(100, command.Variables["@PersonId_0"]);
            Assert.Equal(10, command.Variables["@Age_1"]);
            Assert.Equal(50, command.Variables["@Age_2"]);
            Assert.Equal(60, command.Variables["@Age_3"]);
            Assert.Equal(65, command.Variables["@Age_4"]);
            Assert.Equal(5, command.Variables.Count);
        }

        [Fact]
        public void ExpressionBuilder_Get()
        {
            var command = ConnectionExtensions.Get<Person>(100);

            var sql = "SELECT [PersonId], [Age], [FirstName], [LastName], [DateOfBirth] FROM [Person] WHERE [PersonId] = @PersonId_0";

            Assert.Equal(sql, command.Sql);
            Assert.Equal(100, command.Variables["@PersonId_0"]);
            Assert.Single(command.Variables);
        }

        [Fact]
        public void ExpressionBuilder_Insert()
        {
            var person = new Person();
            person.Age = 20;
            person.PersonId = 1000;
            person.DateOfBirth = DateTime.UtcNow.AddYears(-20);
            person.FirstName = "Foo";
            person.LastName = "Bar";

            var command = ConnectionExtensions.Insert(person);
            var sql = "INSERT INTO [Person] ([Age], [FirstName], [LastName], [DateOfBirth]) VALUES (@Age_0, @FirstName_1, @LastName_2, @DateOfBirth_3)";
            Assert.Equal(sql, command.Sql);
            Assert.Equal(person.Age, command.Variables["@Age_0"]);
            Assert.Equal(person.FirstName, command.Variables["@FirstName_1"]);
            Assert.Equal(person.LastName, command.Variables["@LastName_2"]);
            Assert.Equal(person.DateOfBirth, command.Variables["@DateOfBirth_3"]);
            Assert.Equal(4, command.Variables.Count);
        }

        [Fact]
        public void ExpressionBuilder_Update()
        {
            var person = new Person();
            person.PersonId = 123;
            person.Age = 20;
            person.PersonId = 1000;
            person.DateOfBirth = DateTime.UtcNow.AddYears(-20);
            person.FirstName = "Foo";
            person.LastName = "Bar";

            var command = ConnectionExtensions.Update(person);
            var sql = "UPDATE [Person] SET [Age] = @Age_1, [FirstName] = @FirstName_2, [LastName] = @LastName_3, [DateOfBirth] = @DateOfBirth_4 WHERE [PersonId] = @PersonId_0";
            Assert.Equal(sql, command.Sql);
            Assert.Equal(person.PersonId, command.Variables["@PersonId_0"]);
            Assert.Equal(person.Age, command.Variables["@Age_1"]);
            Assert.Equal(person.FirstName, command.Variables["@FirstName_2"]);
            Assert.Equal(person.LastName, command.Variables["@LastName_3"]);
            Assert.Equal(person.DateOfBirth, command.Variables["@DateOfBirth_4"]);
            Assert.Equal(5, command.Variables.Count);
        }

        [Fact]
        public void ExpressionBuilder_Delete()
        {
            var id = 123;
            var command = ConnectionExtensions.Delete<Person>(id);
            var sql = "DELETE FROM [Person] WHERE PersonId = @PersonId_0";
            Assert.Equal(sql, command.Sql);
            Assert.Equal(id, command.Variables["@PersonId_0"]);
            Assert.Single(command.Variables);
        }
    }
}
