Spot
====

[![Build status](https://ci.appveyor.com/api/projects/status/xmcrac78ci9uj9xt?svg=true)](https://ci.appveyor.com/project/Page/spot)


Spot is a lightweight and easy-to-learn data access library for .NET Core. Using POCOs and a simple filter API, 
Spot generates and executes well-formed SQL commands at runtime. Query results are mapped back to POCOs. Mappings
can also be customized when there are differences between the POCO and the underlying table. 


Installation
============
Simply run the appropriate command below to install from NuGet gallery:

```powershell
PM> Install-Package Spot
PM> Install-Package Spot.StrongName
```

Examples
========

The examples below illustrate interactions with a SQL table called User and a C# POCO:

```sql
CREATE TABLE [dbo].[User](
    [UserId] [int] IDENTITY(1,1) NOT NULL,
    [Name] [nvarchar](100) NOT NULL,
    [DateInserted] [date2] NOT NULL,
    [Active] [bit] NOT NULL
)
```
```c#
public class User
{
   public int UserId { get; set; }
   public string Name { get; set; }
   public bool Active { get; set; }
   public DateTime DateInserted { get; set; }
}
```

Simple SELECT Example
---------------------

Using Spot's expression builder syntax, you can build strongly-typed SQL commands with ease.

```c#
// Return all rows from User table where Active is true.
var filter = Expression.Eq<User>(u => u.Active, true);
var users = cn.GetList<User>(filter);
foreach(var user in users)
{
    Console.WriteLine(user);
}
```

Generated SQL:
```sql
SELECT [UserId], [DateInserted], [Name], [Active] 
FROM [User] 
WHERE [Active] = @Active_0
```

All expression values are properly parameterized and passed at execution. The results from the 
query are automatically mapped to a collection of User objects.


Advanced SELECT Example
-----------------------
You can also combine expressions to form more complex queries:

```c#
// Return all rows from User table where Active is true and DateInserted within 30 days.s
var maxAge = DateTime.UtcNow.AddDays(-30);
var dateFilter = Expression.Gte<User>(u => u.DateInserted, maxAge)
var activeFilter = Expression.Eq<User>(u => u.Active, true);
var filter = Expression.And(dateFilter, activeFilter);

// Sort by Name ascending
var sort = Expression.SortAsc<User>(u => u.Name); 

var users = cn.GetList<User>(filter, sort);
foreach(var user in users)
{
    Console.WriteLine(user);
}

```
Generated SQL:
```sql
SELECT [UserId], [DateInserted], [Name], [Active] 
FROM [User]
WHERE ([DateInserted] >= @DateInserted_0 AND [Active] = @Active_1)
ORDER BY [Name]
```

More [advanced examples](Advanced-Examples) can be found in the documentation.

INSERT, UPDATE, and DELETE Examples
--------

```c#
// Insert a User into the User table and set the generated identity value on the POCO
var user = new User
{
   DateInserted = DateTime.UtcNow,
   Name = "John Smith",
   Active = 1
}
var userId = cn.Insert(user);
```
Generated SQL:
```sql
INSERT INTO [User] 
   ([DateInserted], [Name], [Active])
VALUES
   (@DateInserted_0, @Name_1, @Active_2);
SELECT @UserId_3 = SCOPE_IDENTITY()
```

```c#
// Update an existing User in the User table.
var user = new User
{
   UserId = 1,
   DateInserted = DateTime.UtcNow,
   Name = "Jane Smith",
   Active = 1
};

var userId = cn.Update(user);
```
Generated SQL:
```sql
UPDATE [User] 
SET [DateInserted] = @DateInserted_0, 
    [Name] = @Name_1,
    [Active] = @Active_2
WHERE [UserId] = @UserId_3
```

```c#
// Delete an existing User in the User table.
var userId = 1;
cn.Delete(userId);
```
Generated SQL:
```sql
DELETE FROM [User] WHERE [UserId] = @UserId_0
```


More [advanced examples](Advanced-Examples) can be found in the documentation.

Customized Mappings
===================
You can control how Spot maps a POCO over to a table using a ClassMapper. In the example below, the User class is mapped to a Users table. Additional column-level options are also set within the Map method.

```c#
public class UserClassMapper : ClassMapper<User>
{
    public override void Map()
    {
        // Map the Users table to the User class.
        Table("Users"); 

        // Map the UserId column to the Id property and set the FieldType to AssignedKey.
        // AssignedKey will allow you to supply the Primary Key instead of
        // using an automatically generated identity value.
        Column("UserId", u => u.Id, FieldType.Field);

        // Map the Fullname column to the Name property.
        Column("FullName", u => u.Name, FieldType.Field);

        // Any remaining fields will be mapped.
        base.Map();
    }
}

// Add the ClassMappers during application start-up as follows:
SpotSettings.ClassMappers.Add<UserClassMapper>():
```


License
=======
This library is offered for free under the terms of the [Apache License 2.0](LICENSE).
